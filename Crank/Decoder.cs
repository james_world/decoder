﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Crank
{
    public class Decoder
    {
        private readonly char[] _key = "--------------------------".ToCharArray();

        public string Decode(string cyphertext)
        {
            var plainText = new StringBuilder();
            foreach (char from in cyphertext)
            {
                plainText.Append(DecodeChar(from));
            }
            return plainText.ToString();
        }

        private char DecodeChar(char from)
        {
            char lowerFrom = char.ToLower(from);
            if (lowerFrom == ' ')
                return ' ';
            if (lowerFrom >= 'a' && lowerFrom <= 'z')
            {
                char lowerTo = _key[lowerFrom - 'a'];
                return char.IsUpper(from) ? char.ToUpper(lowerTo) : lowerTo;
            }
            throw new Exception("Unable to handle decoding char: " + from);
        }

        public Decoder Substitute(char from, char to)
        {
            var lowerFrom = char.ToLower(from);
            var lowerTo = char.ToLower(to);

            if (lowerFrom < 'a' || lowerFrom > 'z')
                throw new Exception("Can only substitute from a-z. Invalid char: " + from);

            if (lowerTo < 'a' || lowerTo > 'z')
                throw new Exception("Can only substitute to a-z. Invalid char: " + to);

            ReplaceToCharWithDashIfAlreadyUsed(lowerTo);

            _key[lowerFrom - 'a'] = lowerTo;
            return this;
        }

        private void ReplaceToCharWithDashIfAlreadyUsed(char to)
        {
            for (var i = 0; i < _key.Length; i++)
            {
                if (_key[i] == to)
                    _key[i] = '-';
            }
        }

        public string GetKey()
        {
            return new string(_key);
        }

        public string GetUnusedLetters()
        {
            return _key.Aggregate("abcdefghijklmnopqrstuvwxyz",
                (current, used) => current.Replace(used.ToString(CultureInfo.InvariantCulture), string.Empty));
        }
    }
}