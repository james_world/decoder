﻿using System;

namespace Crank
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Decrypt();
            }
        }

        private static void Decrypt()
        {
            Console.Write("Enter the cyphertext: ");
            var cyphertext = Console.ReadLine();

            Console.WriteLine("\nNow enter pairs of characters to do substitutions. E.g. ab will swap all occurrences of a for b.\nIf the target is already used for another letter, it will be swapped to the new letter. Enter q to stop and get new cyphertext.\n\n");
            string command = "";
            var decoder = new Decoder();

            while (command != "q")
            {
                Console.Write("Enter command: ");
                command = Console.ReadLine();

                if(string.IsNullOrEmpty(command))
                    continue;

                if (command.Length == 2)
                {
                    decoder.Substitute(command[0], command[1]);
                    Console.WriteLine("           Key: " + decoder.GetKey());
                    Console.WriteLine("Unused Letters: " + decoder.GetUnusedLetters());
                    Console.WriteLine("    Cyphertext: " + cyphertext);
                    Console.WriteLine("     Plaintext: " + decoder.Decode(cyphertext));
                    Console.WriteLine();
                }
            }

        }
    }
}
