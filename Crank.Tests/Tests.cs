﻿using System;
using Xunit;
using Xunit.Extensions;

namespace Crank.Tests
{
    public class Tests
    {
        [Fact]
        public void InitiallyDecodesToDashesPreservingSpace()
        {
            const string cyphertext = "abcd efghi jklmnopqrstuvwxyz";
            const string expectedPlaintext = "---- ----- -----------------";
            var sut = new Decoder();

            var plaintext = sut.Decode(cyphertext);

            Assert.Equal(expectedPlaintext, plaintext);
        }


        [Fact]
        public void DecodingPreservesCapitalization()
        {
            const string expectedPlaintext = "JamEs";
            const string cyphertext = "JamEs";
            var sut = new Decoder();
            sut.Substitute('j', 'j');
            sut.Substitute('a', 'a');
            sut.Substitute('m', 'm');
            sut.Substitute('e', 'e');
            sut.Substitute('s', 's');

            var plaintext = sut.Decode(cyphertext);

            Assert.Equal(expectedPlaintext, plaintext);
        }

        [Fact]
        public void CanSubstituteALetter()
        {
            const string cyphertext = "a";
            const string expectedPlaintext = "b";
            var sut = new Decoder();
            sut.Substitute('a','b');

            var plaintext = sut.Decode(cyphertext);

            Assert.Equal(expectedPlaintext, plaintext);
        }

        [Fact]
        public void CanSubstituteFromACapitalLetter()
        {
            const string cyphertext = "a";
            const string expectedPlaintext = "b";
            var sut = new Decoder();
            sut.Substitute('A', 'b');

            var plaintext = sut.Decode(cyphertext);

            Assert.Equal(expectedPlaintext, plaintext);
        }

        [Fact]
        public void CanSubstituteToACapitalLetter()
        {
            const string cyphertext = "a";
            const string expectedPlaintext = "b";
            var sut = new Decoder();
            sut.Substitute('a', 'B');

            var plaintext = sut.Decode(cyphertext);

            Assert.Equal(expectedPlaintext, plaintext);
        }

        [Fact]
        public void CanOnlySubstituteFromLetters()
        {
            var sut = new Decoder();

            var exception = Assert.Throws<Exception>(
                () => sut.Substitute('1', 'B'));

            Assert.Equal("Can only substitute from a-z. Invalid char: 1", exception.Message);
        }

        [Fact]
        public void CanOnlySubstituteToLetters()
        {
            var sut = new Decoder();

            var exception = Assert.Throws<Exception>(
                () => sut.Substitute('a', '1'));

            Assert.Equal("Can only substitute to a-z. Invalid char: 1", exception.Message);
        }

        [Fact]
        public void SubstitutingForAnAlreadyUsedCharacterUsesItAtNewPositionOnly()
        {
            const string cyphertext = "ab";
            const string expectedPlaintext = "b-";
            var sut = new Decoder();
            sut.Substitute('b', 'b');
            sut.Substitute('a', 'b');

            var plaintext = sut.Decode(cyphertext);

            Assert.Equal(expectedPlaintext, plaintext);
        }

        [Fact]
        public void CanGetCurrentSubstitutions()
        {
            var sut = new Decoder();
            sut.Substitute('a', 'b');
            sut.Substitute('z', 'y');

            var substitutions = sut.GetKey();
            const string exepctedKey = "b------------------------y";

            Assert.Equal(exepctedKey, substitutions);
        }

        [Theory]
        [InlineData("ab,cd,ef,gh", "acegijklmnopqrstuvwxyz")]
        public void CanGetListOfUnusedLetters(string substutions, string expectedUnused)
        {
            var sut = new Decoder();
            var substitutionPairs = substutions.Split(',');
            foreach (var pair in substitutionPairs)
            {
                sut.Substitute(pair[0], pair[1]);
            }

            var unused = sut.GetUnusedLetters();

            Assert.Equal(expectedUnused, unused);
        }
    }

   
}
